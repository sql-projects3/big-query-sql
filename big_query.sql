WITH grp_money AS (SELECT  test_grp, studs.st_id AS st_id, money
                   FROM studs AS studs 
                   LEFT JOIN final_project_check AS check 
                   ON studs.st_id = check.st_id
                  ),
                     
     act_grp_money AS (WITH act_st AS (
                                       SELECT st_id AS act_id, SUM(correct) AS num_correct
                                       FROM peas
                                       GROUP BY  st_id
                                       HAVING num_correct >= 10
                                      )
    
                       SELECT test_grp, act_id, money 
                       FROM act_st AS act_st
                       LEFT JOIN (SELECT st_id, money
                       FROM final_project_check) AS check 
                       ON act_st.act_id = check.st_id
                       LEFT JOIN studs AS studs
                       ON act_st.act_id = studs.st_id
                      ),

     act_math_grp_money AS (WITH act_math AS (SELECT st_id AS act_math_id, SUM(correct) AS num_correct
                                              FROM peas
                                              WHERE subject = 'Math'
                                              GROUP BY st_id
                                              HAVING num_correct >= 2
                                             )
                            SELECT test_grp, act_math_id, money 
                            FROM act_math AS act_math
                            LEFT JOIN (SELECT st_id, money
                                       FROM final_project_check 
                                       WHERE subject = 'Math') AS check 
                            ON act_math.act_math_id = check.st_id
                            LEFT JOIN studs AS studs
                            ON act_math.act_math_id = studs.st_id
                           )

SELECT metrics1.grp AS grp, metrics1.group_size AS group_size, ARPU, CR,
       metrics2.act_group_size AS act_group_size, ARPAU, CR_act_st,
       metrics3.act_math_group_size AS act_math_group_size, CR_act_math 
       
FROM  (SELECT grp_money.test_grp AS grp, COUNT(DISTINCT grp_money.st_id) AS group_size,
              SUM(grp_money.money) AS total_money, ROUND(SUM(grp_money.money)/COUNT(DISTINCT grp_money.st_id), 2) AS ARPU,
              ROUND((COUNTIf(DISTINCT grp_money.st_id, grp_money.money > 0)/COUNT(DISTINCT grp_money.st_id)) * 100, 2) AS CR
       FROM grp_money      
       GROUP BY grp_money.test_grp) AS metrics1
       LEFT JOIN (SELECT act_grp_money.test_grp AS grp, COUNT(DISTINCT act_grp_money.act_id) AS act_group_size,
                         ROUND(SUM(act_grp_money.money)/COUNT(DISTINCT act_grp_money.act_id), 2) AS ARPAU,
                         ROUND((COUNTIf(DISTINCT act_grp_money.act_id, act_grp_money.money > 0)/
                         COUNT(DISTINCT act_grp_money.act_id)) * 100, 2) AS CR_act_st
                  FROM act_grp_money      
                  GROUP BY act_grp_money.test_grp) AS metrics2
       ON metrics1.grp = metrics2.grp
       LEFT JOIN (SELECT act_math_grp_money.test_grp AS grp, 
                         COUNT(DISTINCT act_math_grp_money.act_math_id) AS act_math_group_size,
                         ROUND((COUNTIf(DISTINCT act_math_grp_money.act_math_id, act_math_grp_money.money > 0)/
                         COUNT(DISTINCT act_math_grp_money.act_math_id)) * 100, 2) AS CR_act_math
                  FROM act_math_grp_money      
                  GROUP BY act_math_grp_money.test_grp) AS metrics3    
       ON metrics1.grp = metrics3.grp